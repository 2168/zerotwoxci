#pragma once

#include <switch.h>
#include <vector>
#include <string>

namespace tin::util
{
    std::vector<std::string> GetNSPList();
    std::vector<std::string> GetXCIList();
}
