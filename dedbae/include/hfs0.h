#pragma once

typedef struct __attribute__((packed)) {
	/* 0x 0 */ char magic[4]; // "HFS0"
	/* 0x 4 */ uint32_t num_files;
	/* 0x 8 */ uint32_t stringtable_size;
	/* 0x C */ char unknown_0[4]; // Zeroes?
	// File entry table starts at 0x10
} hfs0_header_t;

typedef struct __attribute__((packed)) {
	/* 0x 0 */ uint64_t file_offset;
	/* 0x 8 */ uint64_t file_size;
	/* 0x10 */ uint32_t file_name_offset; // In string table
	/* 0x14 */ uint32_t hashed_region_size;
	/* 0x18 */ char unknown[8]; // zeroes?
	/* 0x20 */ char file_hash[0x20]; // Hash of only the first hashed_region_size bytes of the file
} hfs0_file_entry_t;

// The string table and the file data areas have no real structure; the offsets in the header / file entries tell all.
