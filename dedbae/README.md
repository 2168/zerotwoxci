# dedbae: A library and tools for manipulating Nintendo Switch files

dedbae is a lightweight C library for decrypting, inspecting, modifying, re-encrypting, and even creating file formats used internally by the Nintendo Switch's operating system.

NOTE: If you're running in an environment where standard C stdio will not be available, you must replace src/lib/minIni/dev/minGlue.h with an appropriate substitute.
