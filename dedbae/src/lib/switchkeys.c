#include "switchkeys.h"

#include "utils.h"

#include "minIni/dev/minIni.h"

#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#define DEFAULT_KEYFILE "/.switch/prod.keys"
#define DEFAULT_KEYFILE_NOHOME "/prod.keys"

switchkeys_t switchkeys;

// ishex(), hextoi(), parse_hex_key() taken from hactool © SciresM
static int ishex(char c) {
    if ('a' <= c && c <= 'f') return 1;
    if ('A' <= c && c <= 'F') return 1;
    if ('0' <= c && c <= '9') return 1;
    return 0;
}

static char hextoi(char c) {
    if ('a' <= c && c <= 'f') return c - 'a' + 0xA;
    if ('A' <= c && c <= 'F') return c - 'A' + 0xA;
    if ('0' <= c && c <= '9') return c - '0';
    return 0;
}

bool parse_hex_key(unsigned char *key, const char *hex, unsigned int len) {
    if (strlen(hex) != 2 * len) {
        //fprintf(stderr, "Key (%s) must be %"PRIu32" hex digits!\n", hex, 2 * len);
        return false;
    }

    for (unsigned int i = 0; i < 2 * len; i++) {
        if (!ishex(hex[i])) {
            //fprintf(stderr, "Key (%s) must be %"PRIu32" hex digits!\n", hex, 2 * len);
            return false;
        }
    }

    memset(key, 0, len);

    for (unsigned int i = 0; i < 2 * len; i++) {
        char val = hextoi(hex[i]);
        if ((i & 1) == 0) {
            val <<= 4;
        }
        key[i >> 1] |= val;
    }
    return true;
}

void import_key(char* keyfile, char* keyname, char* output, unsigned int size)
{
    char key_octets[2*size+1]; // +1 for trailing null
    ini_gets("", keyname, "", key_octets, 2*size+1, keyfile);
    if(key_octets[0] == '\0') {
        // Keyfile does not have this key
        // TODO: Return error
        return;
    }
    
    parse_hex_key(output, key_octets, size);
}

void switchkeys_load_keyfile(char* specified_keyfile) {
    char* rel_keyfile = specified_keyfile;
    
    if(rel_keyfile == NULL) {
        char* homedir = getenv("HOME");
        if(homedir == NULL) homedir = getenv("HOMEPATH"); // Because Windows just *has* to be different.
        if(homedir != NULL) {
            rel_keyfile = malloc( strlen(homedir) + strlen(DEFAULT_KEYFILE) + 1 );
            strcpy(rel_keyfile, homedir);
            strcat(rel_keyfile, DEFAULT_KEYFILE);
        }
        else rel_keyfile = DEFAULT_KEYFILE_NOHOME;
    }
    
    char* keyfile = realpath(rel_keyfile, NULL);
    if(keyfile == NULL)
        // TODO: Return error
        return;
    
    import_key(keyfile, "header_key", switchkeys.header, 32);
    
    import_key(keyfile, "key_area_key_application_00", switchkeys.application_key_area[0], 16);
    import_key(keyfile, "key_area_key_application_01", switchkeys.application_key_area[1], 16);
    import_key(keyfile, "key_area_key_application_02", switchkeys.application_key_area[2], 16);
    import_key(keyfile, "key_area_key_application_03", switchkeys.application_key_area[3], 16);
    import_key(keyfile, "key_area_key_application_04", switchkeys.application_key_area[4], 16);
    
    import_key(keyfile, "titlekek_00", switchkeys.titlekeks[0], 16);
    import_key(keyfile, "titlekek_01", switchkeys.titlekeks[1], 16);
    import_key(keyfile, "titlekek_02", switchkeys.titlekeks[2], 16);
    import_key(keyfile, "titlekek_03", switchkeys.titlekeks[3], 16);
    import_key(keyfile, "titlekek_04", switchkeys.titlekeks[4], 16);
    
    free(keyfile);
}
