#include "utils.h"

#include <stdio.h>
#include <stdlib.h>

void bail(char* message) {
    printf("%s\n\n", message);
    exit(1);
}
