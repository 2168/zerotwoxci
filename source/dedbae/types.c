#include "types.h"

char* title_type_to_string(uint8_t type) {
	switch(type) {
		case TITLE_TYPE_SYSTEM_PROGRAM:
			return "System program"; break;
		case TITLE_TYPE_SYSTEM_DATA:
			return "System data archive"; break;
		case TITLE_TYPE_SYSTEM_UPDATE:
			 return "System update"; break;
		case TITLE_TYPE_FIRMWARE_A:
			return "Firmware package A"; break;
		case TITLE_TYPE_FIRMWARE_B:
			return "Firmware package B"; break;
		case TITLE_TYPE_APPLICATION:
			return "Application"; break;
		case TITLE_TYPE_APP_UPDATE:
			return "Application update"; break;
		case TITLE_TYPE_ADDON_CONTENT:
			return "Add-on content"; break;
		case TITLE_TYPE_DELTA:
			return "Delta"; break;
			
	}
}
