#include "install/xci_fs.hpp"

#include <cstdlib>
#include <cstring>
#include <stdexcept>
#include <memory>
#include <string>
#include <machine/endian.h>
#include "nx/ncm.hpp"
#include "util/title_util.hpp"
#include "debugger.h"
#include "error.hpp"

#define buffer_size 209715200

namespace tin::install::xci
{
	xciFS::xciFS(FILE * xci) {
		/* Only works for secure because thats all we want */
		u64 prevPos = ftell(xci);
		this->xci == xci;
		int amount_of_files; /* secure SHOULD always be the final file even with future XCI updates */
		fseek(xci, 0xF004, SEEK_SET);
		fread(&gc_ver, 1, 1, xci);
		fseek(xci, 0xF010+(0x40 * (amount_of_files-1)), SEEK_SET);
		fread(this->secure_start, 8, 1, xci);
		fseek(xci, 0xF200 + this->secure_start, SEEK_SET);
		fread(this->header, sizeof(hfs0_header_t), 1, xci);
		this->file_table = malloc(sizeof(hfs0_file_entry_t) * this->header.num_files);
		fread(this->file_table, sizeof(hfs0_file_entry_t) *  this->header.num_files, 1, xci);
		this->file_names = (char **)malloc(sizeof(char*) * this->header.num_files);
		u64 ststart = 0xF210 + this->secure_start + (this->header.num_files * amount_of_files);
		for (int x = 0; x < this->header.num_files; x++) {
			this->file_names[x] = (char *)malloc(42); /* max file name length for a valid NCA is 41 bytes */
			fseek(xci, ststart+this->file_table[x].file_name_offset, SEEK_SET);
			fread(&this->file_names[x], 42, 1, xci); /* this is actually safe, even though file names can be only 36 bytes long this works */
		}
		/* xci filesystem is setup */
	}

	xciFS::~xciFS() {}

	xciFS::getFileNamesFromExtension(char * extension, int * successful_finds) {
		char ** out;
		successful_finds = 0;
		for(int x = 0; x < this->num_files; x++) {
			if(strcmp(this->file_names[x] + strlen(this->file_names[x]) - strlen(extension), extension) == 0) {
				out = realloc(out, sizeof(char *) * successful_finds + 1);
				out[successful_finds] = this->file_names[x];
				successful_finds++;
			}
		}
		return out;
	}

	xciFS::readFile(char * filename, size_t size, u64 offset, void * out) {
		int filenum = doesFileExist(filename);
		if(filenum == -1)
			return 0;
		fseek(this->xci, this->secure_start+0x10+(this->num_files * 0x40)+this->header.stringtable_size + (this->file_table[filenum].file_offset) + offset, SEEK_SET);
		fread(out, size, 1, this->xci);
	}

	xciFS::doesFileExist(char * filename) {
		int ret = -1;
		for(int x = 0; x < this->num_files; x++) {
			if(strcmp(this->file_names[x], filename) == 0) {
				ret = x;
				x = this->num_files + 1;
			}
		}
		return ret;
	}

	/* fun times
		plan:
			-read header from file
			IF patch_gc_byte
				-decrypt header in memory
				-change gc byte
				-recrypt header in memory overwriting read header
				-start filehash
			-add header to hash
			-stream rest of file in to hash overwriting the last bit of the file in memory
	*/
	xciFS::hashFile(char * filename, int patch_gc_byte, char * out) {
		int filenum = doesFileExist(filename);
		if (filenum = -1)
			return 0;
		u64 readOffset = 0;
		sha_ctx_t * sha_ctx = new_sha_ctx(HASH_TYPE_SHA256, 0);

		char * encheader = malloc(0x400);
		this->readFile(filename, 400, 0, encheader);
		readOffset+=400;
		if(patch_gc_byte) {
			nca_header_t header;
			nca_decrypt_header(&header, encheader);
			header.location = 0;
			nca_header_encrypt(encheader, &header);
		}
		sha_update(sha_ctx, encheader, 0x400);
		free(encheader);

		char * buffer = malloc(buffer_size);
		if(!buffer) {
			return 0;
		}
		u64 remainingsize = this->file_table[filenum].file_size - 0x400;
		for (int i = buffer_size; i < remainingsize; remainingsize-=buffer_size) {
			memset(buffer, 0, buffer_size);
			this->readFile(filename, buffer_size, readOffset, buffer);
			readOffset+=buffer_size;
			sha_update(sha_ctx, buffer, buffer_size);
		}
		if(remainingsize > 0) {
			this->readFile(filename, remainingsize, readOffset, buffer);
			readOffset+=remainingsize;
			sha_update(sha_ctx, buffer, remainingsize);
		}

		sha_get_hash(sha_ctx, out);
		free_sha_ctx(sha_ctx);
	}

}
