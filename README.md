# ZeroTwoXCI
A homebrew application for the Nintendo Switch used to manage XCI titles.

DISCLAIMER: I take zero responsibility for any bans, damage, nuclear explosions, or anything else the use of ZeroTwoXCI may cause. Proceed with caution!

## Installation
1. Install FS sig patches, there are two ways of doing this:
    - Using [ReiNX](https://github.com/Reisyukaku/ReiNX)
2. Place the zerotwoxci nro in the "switch" folder on your sd card, and run using the homebrew menu.

## Download
For the latest NRO go to [tags](https://gitlab.com/2168/zerotwoxci/tags).

## Usage
### For XCIs
1. Place XCI in ``/tinfoil/zerotwoxci/``
2. Put prod.keys keyfile on the root of your sd
3. Launch ZeroTwoXCI via the homebrew menu and install your XCIs.

## Donate
[Buy me a coffee](ko-fi.com/elisezerotwo )

## Credits
This project was developed by EliseZeroTwo.

I'd like to thank the following people for their help or for using some of their code.

Roothorick for Dedbae

Adubbz for Tinfoil

SciresM for hactool code (specifically the hfs0 header functions)

The-4N for helping me

NullModel for a Hex To ASCII converter https://github.com/ENCODE-DCC/kentUtils/commits?author=NullModel

Calypso for some code
